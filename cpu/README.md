# Semestrální projekt č.1: Jednocyklový procesor

## Základní návrh
Navrhněte a popište v jazyce Verilog jednoduchý 32-bitový procesor. Procesor musí podporovat následující instrukce: add, sub, and, or, slt, addi, lw, sw, beq, jal, jr, addu.qb a addu_s.qb. Procesor po resetu začne vykonávat instrukce od adresy 0x00000000. Procesor je připojený k instrukční a datové paměti dle obrázku níže.

![IM1](./media/IM1.png)

![IM2](./media/IM2.png)

Poznámka: Písmena d, s a t odkazují na hodnoty uložené v těchto registrech.

## Rozšířený návrh

Přidejte do procesoru podporu pro následující instrukce: sllv, srlv, srav a j.
![IM3](./media/IM3.png)

Pozor na pořadí operandů! Poznámka: Odevzdávací systém testuje tyto instrukce postupně. V případě, že narazí na nesprávnou implementaci, nepokračuje v testování a vypíše počet bodů. Protože se jedná o rozšířený návrh, již neposkytuje nápovědu.

## Program

Napište program, který vzestupně seřadí pole celých čísel (jedno číslo má délku 32 bitů, může být i záporné). Program bude používat rutinu sort, která přijímá 2 argumenty: adresu pole a počet prvků pole. V jazyce C by této rutině odpovídala funkce s následujícím prototypem:

```void sort(int *address, int N);```

Můžete použít libovolný algoritmus řazení. Používejte volací konvenci O32. Předpokládejte, že adresa pole čísel je uložena v paměti na adrese 0x0000000C a počet prvků v poli je uložen v paměti na adrese 0x00000008. Přeložte tento program do strojového kódu.

## Hodnocení semestrální práce

![IM4](./media/IM4.png)

Semestrální práce by měla být odevzdána do 20.11.2020. Každý započatý týden zpoždění je penalizován ztrátou dvou bodů. Odevzdání po 13. týdnu není možné.

>Váš program musí být otestován na Vašem CPU, jinak se nehodnotí. Jinými slovy, 9 bodů za program můžete získat pouze tehdy, pokud odevzdáte popis CPU, na kterém tento program běží a generuje očekávané výsledky (seřadí pole v paměti). Body za program můžete získat i tehdy, není-li Váš CPU zcela korektní.

## Způsob odevzdání semestrální práce

Vaše řešení (zabalené v zip archivu) odevzdejte přes http://biaps.fit.cvut.cz/first_semestral_project/index.php V případě problémů, můžete Vaše řešení poslat na email cvičícího (pokud jste nevyčerpali všechny pokusy).

CELKEM je k dispozici 10 pokusů. V systému se uchovává pouze poslední pokus.

## Základní a rozšířený návrh

Popis CPU uložte do jednoho souboru. Název souboru musí být následující: Surname_GivenName_CPU.v (bez diakritiky, čili bez háčků a čárek). Všechny moduly, které potřebujete, popište v rámci tohoto souboru.

Použijte následující šablonu. Nazvy vstupů a výstupů neměňte.
```
module processor( input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem
                );
    //... write your code here ...
endmodule

//... add new modules here ...
```
>Odevzdávejte pouze popis CPU. Nepřikládejte popis dalších komponent (data memory, instruction memory, etc.).

## Program

Uložte Váš program v jazyku symbolických adres do jednoho souboru. Jméno souboru musí být následující: **Surname_GivenName_prog1.asm** . Program ve strojovém kódu (hexadecimální formát) musí být uložen v dalším souboru, kde každá instrukce začíná na novém řádku. Jméno souboru musí být následující: **Surname_GivenName_prog1.hex**

>Všechny odevzdávané soubory zabalte do zip archivu. Nevytvářejte složky a podsložky v tomto archívu. Archiv musí obsahovat POUZE odevzdávané soubory.

## Pomůcka

Pro vygenerování strojového kódu lze použít simulátor MARS používaný na cvičeních. Je vhodné použít textový formát, kde každá instrukce je uvedena hexadecimálně, samostatně v každém řádku souboru. Postup ilustruje následující obrázek. Pozor, je MARS používá instrukční sadu MIPS32, která se od naší nepatrně liší - viz výše uvedenou definici instrukcí.

![IM5](./media/IM5.png)

Dále můžete použít následující moduly a modifikovat je dle libosti. Nebudou součástí Vašeho řešení ani je neodevzdávejte.

```
module top (	input         clk, reset,
		output [31:0] data_to_mem, address_to_mem,
		output        write_enable);

	wire [31:0] pc, instruction, data_from_mem;

	inst_mem  imem(pc[7:2], instruction);
	data_mem  dmem(clk, write_enable, address_to_mem, data_to_mem, data_from_mem);
	processor CPU(clk, reset, pc, instruction, write_enable, address_to_mem, data_to_mem, data_from_mem);
endmodule

//-------------------------------------------------------------------
module data_mem (input clk, we,
		 input  [31:0] address, wd,
		 output [31:0] rd);

	reg [31:0] RAM[63:0];

	initial begin
		$readmemh ("memfile_data.hex",RAM,0,63);
	end

	assign rd=RAM[address[31:2]]; // word aligned

	always @ (posedge clk)
		if (we)
			RAM[address[31:2]]<=wd;
endmodule

//-------------------------------------------------------------------
module inst_mem (input  [5:0]  address,
		 output [31:0] rd);

	reg [31:0] RAM[63:0];
	initial begin
		$readmemh ("memfile_inst.hex",RAM,0,63);
	end
	assign rd=RAM[address]; // word aligned
endmodule
```

Pro simulaci můžete použít následující modul:

```
module testbench();
	reg         clk;
	reg         reset;
	wire [31:0] data_to_mem, address_to_mem;
	wire        memwrite;

	top simulated_system (clk, reset, data_to_mem, address_to_mem, write_enable);

	initial	begin
		$dumpfile("test");
		$dumpvars;
		reset<=1; # 2; reset<=0;
		#100; $finish;
	end

	// generate clock
	always	begin
		clk<=1; # 1; clk<=0; # 1;
	end
endmodule
```