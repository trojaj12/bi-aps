module processor( input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem
                );

        wire        RegWrite, RegDst, ALUSrc, Branch, MemToReg, PCSrcJal, PCSrcJr;
        wire        zero;
        wire [3:0]  ALUControl;
        wire [4:0] MX_1__MX_0, MUX4__RM_A3;
        wire [31:0] RM_rd1__ALU_a, RM_rd2__MX_2, MX_3__MX5_0, MUX5__RM_WD3, ALU_res__DM_a, MX_2__ALU_2, EXP_out__MX_2, PC_after_4_added, PC_after_BR_adder, PC_nove;
        wire [31:0] RM_rd1__ALU_a, RM_rd2__MX_2, MX_3__MX5_0, MUX5__RM_WD3, ALU_res__DM_a, MX_2__ALU_2, EXP_out__MX_2, PC_after_4_added, PC_after_BR_adder, PC_nove;
        reg [31:0] PC_tmp;

    ControlUnit     CU  ( instruction[31:26], instruction[5:0], instruction[10:6], RegWrite,
                            RegDst, ALUSrc, Branch, WE, MemToReg, PCSrcJal, PCSrcJr, ALUControl );

    RegisterMemory  RM  ( instruction[25:21], instruction[20:16], MUX4__RM_A3, RegWrite, MUX5__RM_WD3 ,
                            clk, RM_rd1__ALU_a, RM_rd2__MX_2);
                        assign data_to_mem = RM_rd2__MX_2;


    ALU             ALU ( RM_rd1__ALU_a, MX_2__ALU_2, ALUControl, zero, address_to_mem );

    Expand_16_32        EXP ( instruction[15:0], EXP_out__MX_2 );

    Multiplexor #(5)    MX_1( instruction[20:16], instruction[15:11], RegDst ,MX_1__MX_0);

    Multiplexor         MX_2( RM_rd2__MX_2,EXP_out__MX_2 , ALUSrc , MX_2__ALU_2 );

    Multiplexor         MX_3( data_from_mem, address_to_mem, ~MemToReg, MX_3__MX5_0);


    Multiplexor #(5)    MX_4( MX_1__MX_0, 5'd 31, PCSrcJal, MUX4__RM_A3);
    Multiplexor         MX_5( MX_3__MX5_0, PC_after_4_added, PCSrcJal, MUX5__RM_WD3);


    Adder               ADPC(PC_tmp, 'h 4, PC_after_4_added);
    Adder               ADBR(PC_after_4_added, (EXP_out__MX_2 << 2), PC_after_BR_adder);


    LastMux             LM  ( RM_rd1__ALU_a, { PC_after_4_added[31:28],  instruction[25:0], 2'b00 } ,PC_after_BR_adder,PC_after_4_added, PCSrcJr, PCSrcJal, zero && Branch, PC_nove );
    //Multiplexor         MX6( PC_after_4_added, PC_after_BR_adder ,zero && Branch ,PC_nove );
    always@( posedge clk )
    begin
		if( reset == 1 )
          	PC_tmp = 32'h 00;
      	else
      		PC_tmp = PC_nove;
    end

    assign PC = PC_tmp;

endmodule