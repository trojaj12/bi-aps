module Adder
    (
        input  [31:0] A, B,
        output [31:0] Res
    );
    assign Res = A + B;
endmodule
