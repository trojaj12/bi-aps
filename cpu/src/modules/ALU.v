module ALU
    (
        input  [31:0] ScrA, ScrB,
        input  [3:0] ALUControl,
        output reg Zero,
        output reg [31:0] ALUResult
    );

    always@ ( * ) begin
        case ( ALUControl )
            4'b 0010: ALUResult = ScrA + ScrB;
            4'b 0110: ALUResult = ScrA - ScrB;
            4'b 0000: ALUResult = ScrA & ScrB;
            4'b 0001: ALUResult = ScrA | ScrB;
            4'b 0011: ALUResult = ScrA ^ ScrB;
            4'b 0111: ALUResult = $signed(ScrA) < $signed(ScrB);
            4'b 1000://4 unsigned add
                begin
                    ALUResult[7:0]   = ScrA[7:0]   + ScrB[7:0];
					ALUResult[15:8]  = ScrA[15:8]  + ScrB[15:8];
					ALUResult[23:16] = ScrA[23:16] + ScrB[23:16];
					ALUResult[31:24] = ScrA[31:24] + ScrB[31:24];
                end
            4'b 1001://4 unsigned satur add
                begin
                    ALUResult[7:0] 	 = ( ScrA[7:0] + ScrB[7:0] ) < ScrA[7:0]
                                            ? 255
                                            : ( ScrA[7:0] + ScrB[7:0] );

					ALUResult[15:8]  = ( ScrA[15:8] + ScrB[15:8] ) < ScrA[15:8]
                                            ? 255
                                            : ( ScrA[15:8] + ScrB[15:8] );

					ALUResult[23:16] = ( ScrA[23:16] + ScrB[23:16] ) < ScrA[23:16]
                                            ? 255
                                            : ( ScrA[23:16] + ScrB[23:16] );

					ALUResult[31:24] = ( ScrA[31:24] + ScrB[31:24] ) < ScrA[31:24]
                                            ? 255
                                            : ( ScrA[31:24] + ScrB[31:24] );
                end
            default:
                ALUResult = ScrA + ScrB;
        endcase
    end

    always@( ALUResult )
        Zero = ( ALUResult == 0 ) ? 1 : 0;

endmodule
