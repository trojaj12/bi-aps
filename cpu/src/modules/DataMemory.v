module DataMemory
    (
		input  [31:0] A, WD,
        input  WE,
        input  clk,
		output [31:0] rd
    );

	reg [31:0] RAM[63:0];

	initial begin
		$readmemh ("../data/memfile_data.hex",RAM,0,63);
	end

	assign rd=RAM[A[31:2]]; // word aligned

	always @ (posedge clk)
		if (WE)
			RAM[A[31:2]]<=WD;

endmodule
