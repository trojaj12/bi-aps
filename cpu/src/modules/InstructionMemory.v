module InstructionMemory
    (
        input  [5:0]  A,
		output [31:0] RD
    );

    reg [31:0] RAM[63:0];
	initial begin
		$readmemh ("../data/memfile_inst.hex",RAM,0,3);
	end
	assign RD=RAM[A]; // word aligned
endmodule