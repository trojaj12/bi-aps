module Multiplexor # ( parameter width = 32)
    (
        input [width-1:0] d0,d1,
        input select,
        output [width-1:0] out
    );

    assign out = select ? d0 : d1;

endmodule

module LastMux
    (
        input [31:0] d0,d1,d2,d3,
        input s0, s1, s2,
        output reg [31:0] ven
    );

    always@ (*) begin
        if( s0 )
            ven = d0;
        else if( s1 )
            ven = d1;
        else if( s2 )
            ven = d2;
        else
            ven = d3;
    end

endmodule
