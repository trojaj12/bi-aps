#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#include <unistd.h>
#include <time.h>

#define RGB_SIZE 3

typedef struct TPicture
{
    size_t width;
    size_t height;
    size_t depth;
} TPicture;
typedef struct THistogram
{
    unsigned int i0_50;
    unsigned int i51_101;
    unsigned int i102_152;
    unsigned int i153_203;
    unsigned int i204_255;
} THistogram;
typedef struct TPixel
{
    int r;
    int g;
    int b;
} TPixel;
TPixel returnPixel(unsigned char ** pictureArray, size_t i, size_t j)
{
    TPixel pixel;
    pixel.r = pictureArray[i][j];
    pixel.g = pictureArray[i][j+1];
    pixel.b = pictureArray[i][j+2];
    return pixel;
}
bool ScanMetaData( FILE * inputFile, TPicture * metaData )
{
    fseek( inputFile , 3 , SEEK_SET );
    if ( fscanf( inputFile , "%lu %lu\n%lu\n" , &(metaData->width) , &(metaData->height) , &(metaData->depth) ) !=3 )
        return false;
    return true;
}
void Deallock( unsigned char ** pictureArray , const TPicture * metaData )
{
    if( !pictureArray )
        return;
    for( size_t i = 0 ; i < metaData->height ; ++i )
        free(pictureArray[i]);
    free(pictureArray);
}
unsigned int CountGrey( TPixel p )
{
    return round( 0.2126*(p.r) + 0.7152*(p.g) + 0.0722*(p.b));
}
void UpdateHistogram( THistogram * histogram , unsigned int y )
{
    if ( y <= 50 )
        ++( histogram->i0_50 );
    else if ( y <= 101 )
        ++( histogram->i51_101 );
    else if ( y <= 152 )
        ++( histogram->i102_152 );
    else if ( y <= 203 )
        ++(histogram->i153_203 );
    else
        ++( histogram->i204_255 );
}
bool WriteHistogram(const THistogram * histogram)
{
    FILE * outputFile = fopen("output.txt","w");
    if(fprintf(outputFile,"%d %d %d %d %d", histogram->i0_50, histogram->i51_101, histogram->i102_152, histogram->i153_203, histogram->i204_255) != 5)
        return false;
    fclose(outputFile);
    return true;
}
void PrintHistogram(const THistogram * histogram)
{
    printf("%d\n%d\n%d\n%d\n%d\n",histogram->i0_50, histogram->i51_101, histogram->i102_152, histogram->i153_203, histogram->i204_255);
}
bool WriteSameData( unsigned char ** pictureArray , const TPicture * metaData )
{
    FILE * outputFile = fopen("output.ppm","wb");
    fprintf(outputFile,"P6\n%ld\n%ld\n%ld\n",  metaData->width, metaData->height, metaData->depth);

    for( size_t i = 0 ; i < metaData->height ; ++i )
        fwrite(pictureArray[i], sizeof ( unsigned char ) , metaData->width * RGB_SIZE, outputFile);
    fclose(outputFile);
    return true;
}
bool ScanImage( unsigned char ** pictureArray , TPicture * metaData , FILE * inputFile )
{
    for( size_t i = 0 ; i < metaData->height ; ++i )
    {
        pictureArray[i] = ( unsigned  char * ) malloc( metaData->width * RGB_SIZE );
        if ( fread( pictureArray[i] , sizeof ( unsigned char ), metaData->width * RGB_SIZE, inputFile ) == 0)
            return false;
    }
    fclose(inputFile);
    return true;
}
int saturate(int k)
{
    if(k<0)
        return 0;
    if(k > 255)
        return 255;
    return k;
}
int main( int argc , char ** argv )
{

    struct timespec start, stop;
    clock_gettime( CLOCK_REALTIME, &start);

    FILE * inputFile;
    TPicture metaData;
    THistogram histogram;
    histogram.i0_50 = histogram.i51_101 = histogram.i102_152 = histogram.i153_203 = histogram.i204_255 = 0;

    //TEST FILE
    if ( argc != 2 )
        return 1;
    inputFile = fopen( argv[1] , "rb" );
    //READ HEADER
    fseek( inputFile , 3 , SEEK_SET );
    fscanf( inputFile , "%lu %lu\n%lu\n" , &(metaData.width) , &(metaData.height) , &(metaData.depth) );
    //SCAN FILE
    unsigned char ** pictureArray = ( unsigned  char ** ) malloc( metaData.height * sizeof( unsigned char * ) );
    for( size_t i = 0 ; i < metaData.height ; ++i )
    {
        pictureArray[i] = ( unsigned  char * ) malloc( metaData.width * RGB_SIZE );
        if ( fread( pictureArray[i] , sizeof ( unsigned char ), metaData.width * RGB_SIZE, inputFile ) == 0)
            return false;
    }
    fclose(inputFile);

    //WRITE HEADER
    FILE * outputFile = fopen("output.ppm","wb");
    fprintf(outputFile,"P6\n%ld\n%ld\n%ld\n",  metaData.width, metaData.height, metaData.depth);

    //! write first row
    for( size_t i = 0; i < metaData.width * RGB_SIZE ; i+=3 )
    {
        TPixel p = returnPixel(pictureArray, 0, i);
        UpdateHistogram(&histogram, CountGrey(p));
        fprintf(outputFile, "%c%c%c", (unsigned char)p.r, (unsigned char)p.g, (unsigned char)p.b);
    }


    for( size_t i = 1 ; i < metaData.height - 1 ; ++i)
    {
        //! write first pixel
        fprintf(outputFile, "%c%c%c", pictureArray[i][0], pictureArray[i][1], pictureArray[i][2]);
        TPixel p = returnPixel(pictureArray, i, 0);
        UpdateHistogram(&histogram, CountGrey(p));
        for( size_t j = 3 ; j < (metaData.width-1) * RGB_SIZE ; j+=3)
        {
            //! count value of new pixel
            TPixel cp = returnPixel(pictureArray, i, j);
            TPixel up = returnPixel(pictureArray, i-1, j);
            TPixel lp = returnPixel(pictureArray, i, j - 3);
            TPixel rp = returnPixel(pictureArray, i, j + 3);
            TPixel dp = returnPixel(pictureArray, i+1, j);
            TPixel newPixel;
            newPixel.r = saturate(5*cp.r - (up.r + lp.r + rp.r + dp.r));
            newPixel.g = saturate(5*cp.g - (up.g + lp.g + rp.g + dp.g));
            newPixel.b = saturate(5*cp.b - (up.b + lp.b + rp.b + dp.b));
            //! write value of new pixel
            fprintf(outputFile, "%c%c%c", (unsigned char)newPixel.r, (unsigned char)newPixel.g, (unsigned char)newPixel.b);
            UpdateHistogram(&histogram, CountGrey(newPixel));
        }
        //! write last pixel
        fprintf(outputFile, "%c%c%c", pictureArray[i][(metaData.width -1) * RGB_SIZE], pictureArray[i][(metaData.width -1) * RGB_SIZE + 1], pictureArray[i][(metaData.width -1) * RGB_SIZE+2]);
        p = returnPixel(pictureArray, i, (metaData.width -1) * RGB_SIZE);
        UpdateHistogram(&histogram, CountGrey(p));
    }

    //! write last row
    for( size_t i = 0; i < metaData.width * RGB_SIZE ; i+=3 )
    {
        TPixel p = returnPixel(pictureArray, metaData.height-1, i);
        UpdateHistogram(&histogram, CountGrey(p));
        fprintf(outputFile, "%c%c%c", (unsigned char)p.r, (unsigned char)p.g, (unsigned char)p.b);
    }

    fclose(outputFile);

    //dealoc
    for( size_t i = 0 ; i < metaData.height ; ++i )
        free(pictureArray[i]);
    free(pictureArray);

    //write histogram
    FILE * outputFileTXT = fopen("output.txt","w");
    fprintf(outputFileTXT,"%d %d %d %d %d", histogram.i0_50, histogram.i51_101, histogram.i102_152, histogram.i153_203, histogram.i204_255);
    fclose(outputFileTXT);


    clock_gettime( CLOCK_REALTIME, &stop);
    double accum = ( stop.tv_sec - start.tv_sec )*1000.0 + ( stop.tv_nsec - start.tv_nsec )/ 1000000.0;
    printf( "Time: %.6lf ms\n", accum );

    return 0;
}