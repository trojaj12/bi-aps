# BI-APS
Toto je repozitář z předmětu [BI-APS (Archtektura počítatčových systémů)](https://courses.fit.cvut.cz/BI-APS/)

V tomto repozitáři najdete 2 semestrální úlohy.
1) [Jednocyklový procesor](./cpu/README.md)
2) [Cache](./cache/README.md)
